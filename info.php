<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">

<?php require 'partials/menuprincipal.php' ?>
<div class="imagen-f">
    <div class="container">
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2"></div>
            <div class="col-lg-8 col-md-8 col-sm-8 div-contact">
                <h4 class="info" align="justify">
                    WibCode es un proyecto enfocado ayudar a personas que se estan iniciando en la programación y necesitan
                    ayuda para realizar sus proyectos, utilizamos los lenguajes principales que son la base del aprendizaje
                    para los desarrolladores.
                    Todo esto con el fin de que logres tus objetivos de aprendizaje al revisar y comprender las estructuras
                    del Lenguaje de Programación.
                </h4>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2"></div>
        </div>
    </div>
</div>

<?php require 'partials/footer.php' ?>