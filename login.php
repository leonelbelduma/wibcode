
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<?php

require 'partials/menuprincipal.php';
require 'conexion.php';

if (!empty($_POST['email']) && !empty($_POST['password'])) {

    $passw =  htmlspecialchars($_POST['password'], ENT_QUOTES);
    $consulta = "SELECT  id, names,email,password    FROM users 
    WHERE email = '$_POST[email]' ";
    $resultado =  mysqli_query($conexion, $consulta);
    $file = mysqli_num_rows($resultado);
    $row = mysqli_fetch_assoc($resultado);
    if ($file > 0) {
        if (password_verify($passw, $row['password'])) {
            $_SESSION['user_id'] = $row['id'];
           // header('Location: addproyec.php');
            echo "<script>location.href=' addproyec.php?removido=true';</script>";
        } else {
            $message =  "Usuario y/o contrasena no coinciden.";
        }
    } else {
        $message =  "Usuario y/o contrasena no coinciden.";
    }
    mysqli_close($conexion);
}

?>
<br>
<br>
<div class="imagen-f">
    <div class="container">
        <div class="row">
            <div class="col-lg-4"></div>
            <div class="col-lg-4">
                <br>
                <br>
                <div class="card card-container">
                    <div class="card-header"><h5 align="center">Login</h5></div>
                    <div class="card-body">
                        <img src="assets/img/avatar.png" class=" profile-img-card" width="190" height="50" alt="">
                        <form class="" method="POST" action="">
                            <div class="form-group">
                                <input id="email" type="email" class="form-control" name="email" placeholder="Email" required autocomplete="email" autofocus>
                            </div>
                            <div class="form-group">
                                <input id="password" type="password" class="form-control" name="password" placeholder="Password" required autocomplete="current-password">
                                <?php if (!empty($message)) : ?><p style=" color:indianred;"> <?= $message ?></p> <?php endif; ?>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-naranja btn-block">Iniciar sesion</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-4"></div>
        </div>
    </div>
</div>
<?php require 'partials/footer.php' ?>