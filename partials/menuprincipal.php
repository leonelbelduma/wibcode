<?php
session_start();
require 'conexion.php';
$sesion =  null;
$nombre_user  = null;
$estado  = 'none';
$estalogin = '';

if (isset($_SESSION['user_id']) == true) {
    $sesion = $_SESSION['user_id'];
    $consultaU = "SELECT * FROM  users  where id = '$sesion'";
    $resultadoU =  mysqli_query($conexion, $consultaU);
    while ($fila = mysqli_fetch_array($resultadoU)) {
        $nombre_user =  $fila['names'];
        $estado  = '';
        $estalogin = 'none';
    }
    /* $estado  = '';
    $estalogin = 'none';*/
} else {
    // echo "Te perdiste de pagina"; 
}
//se debe cerrar la conexion
$conexion = null;
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="gb18030">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="assets/fontawesome-free/css/all.css" />
    <link rel="stylesheet" type="text/css" href="assets/MDB-Free/css/mdb.min.css" />
    <link rel="shortcut icon" href="assets/img/favicon.png " />
    <title>WibCode</title>
</head>

<body>
    <nav id="menu" class="navbar navbar-expand-sm bg-light navbar navbar-dark bg-primary fixed-top"> <a class="navbar-brand" href="index.php"><img src="assets/img/logo.png" width="150" height="30" alt=""></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">

                <li class="nav-item">
                    <a class="nav-link" href="/index/"><i class="fas fa-home"></i> <b>Probando</b></a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="index.php"><i class="fas fa-home"></i> <b>Inicio</b></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="descargas.php"><i class="fas fa-download"></i> <b>Descargas</b></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="contactanos.php"><i class="fas fa-phone"></i> <b>Contactanos</b></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="info.php"><i class="fas fa-question"></i> <b>Nosotros</b></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="addproyec.php" style="display:<?php echo $estado ?>"><i class="fas fa-file-archive"></i> <b>Proyecto</b></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="register.php" style="display:<?php echo $estado ?>"><i class="fas fa-users"></i> <b>Usuarios</b></a>
                </li>
            </ul>
            <ul class="navbar-nav flex-row ml-md-auto d-none d-md-flex">
                <li class="nav navbar-nav float-md-right">
                    <a type="button" class="btn btn-naranja" href="login.php" style="display: <?php echo $estalogin ?>"><b>Ingresar</b></a>
                </li>
            </ul>
            <ul class="navbar-nav flex-row ml-md-auto d-none d-md-flex">
                <li class="nav navbar-nav float-md-right">
                    <a class="avatar" style="display: <?php echo $estado ?>"><img src="../assets/img/avatar.png"></a>
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="display: <?php echo $estado ?>">Bienvenido, <?php echo $nombre_user ?></b></a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="edit_user.php" style="display: <?php echo $estado ?>"><i class="fas fa-edit"></i> Editar perfil</a>
                        <a class="dropdown-item" href="close.php" style="display: <?php echo $estado ?>"><i class="fas fa-door-open"></i> Cerrar Sesión</a>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
</body>
<script type="text/javascript" src="assets/js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="assets/js/popper.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/datatables.min.js"></script>
<script type="text/javascript" src="assets/fontawesome-free/fontawesome.min.js"></script>

</html>