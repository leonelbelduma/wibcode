<?php



session_name("AMAZONID");
session_start();
/*
    require_once 'config/db.php';
    require_once 'config/parameters.php';*/
require_once 'wib_autoload.php';

//require_once 'views/layout/cabecera.php';
/*/
    function show_error(){
        $error = new errorController();
        $error->index();
    }*/
/*
if (isset($_GET['controller'])) {
    $nombre_controlador = $_GET['controller'] . 'Controller';
} elseif (!isset($_GET['controller']) && !isset($_GET['action'])) {
    $nombre_controlador = controller_default;
} else {
    //  show_error();
    exit();
}*/


if (class_exists($nombre_controlador)) {
    $controlador = new $nombre_controlador();

    if (isset($_GET['action']) and method_exists($controlador, $_GET['action'])) {
        $action = $_GET['action'];
        $controlador->$action();
    } elseif (!isset($_GET['controller']) && !isset($_GET['action'])) {
        $action_default = action_default;
        $controlador->$action_default();
    } else {
      //  show_error();
    }
} else {
   // show_error();
}

require_once 'views/layout/footer.php';





//require 'partials/menuprincipal.php' 






?>



<!--div class="">
    <br>
    <img src="assets/img/background.jpg" width="100%" height="auto" alt="">
    <div class="row">
        <div class="col-lg-2 col-md-2 col-sm-2"></div>
        <div class="col-lg-8 col-md-8 col-sm-8 texto">
            <h4 align="center">
                Si eres estudiante y necesitas un sitio web básico o una aplicación de escritorio,
                este es el lugar apropiado te brindamos asesoría en tus proyectos,
                con el objetivo que logres tus metas de aprendizaje.
            </h4>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-2"></div>
    </div>
    <div class="container">
        <br>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <h1 align="center"><b>Lenguajes de Programación utilizados</b></h1>
                <br>
                <h6 align="center">
                    Utilizamos los siguientes lenguajes de programación ya que son los más
                    usados para la creación de sistemas.
                </h6>
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="card card-profile cambiar">
                                <br>
                                <div class="card-avatar">
                                    <a> <img class="img" src="assets/img/lenguajes/1.png"> </a>
                                </div>
                                <br>
                                <div class="table">
                                    <h4 class="card-caption">Java</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card card-profile cambiar">
                                <br>
                                <div class="card-avatar">
                                    <a> <img class="img" src="assets/img/lenguajes/2.png"> </a>
                                </div>
                                <br>
                                <div class="table">
                                    <h4 class="card-caption">PHP</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card card-profile cambiar">
                                <br>
                                <div class="card-avatar">
                                    <a> <img class="img" src="assets/img/lenguajes/3.png"> </a>
                                </div>
                                <br>
                                <div class="table">
                                    <h4 class="card-caption">CSharp</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card card-profile cambiar">
                                <br>
                                <div class="card-avatar">
                                    <a> <img class="img" src="assets/img/lenguajes/4.png"> </a>
                                </div>
                                <br>
                                <div class="table">
                                    <h4 class="card-caption">Java Script</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <h1 align="center"><b>Bases de Datos utilizadas</b></h1>
                <br>
                <h6 align="center">
                    Utilizamos las siguientes bases de datos ya que son los más
                    usados para la creación de sistemas.
                </h6>
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="card card-profile cambiar">
                                <br>
                                <div class="card-avatar">
                                    <a> <img class="img" src="assets/img/bd/1.png"> </a>
                                </div>
                                <br>
                                <div class="table">
                                    <h4 class="card-caption">Oracle</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card card-profile cambiar">
                                <br>
                                <div class="card-avatar">
                                    <a> <img class="img" src="assets/img/bd/2.png"> </a>
                                </div>
                                <br>
                                <div class="table">
                                    <h4 class="card-caption">MySQL</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card card-profile cambiar">
                                <br>
                                <div class="card-avatar">
                                    <a> <img class="img" src="assets/img/bd/3.png"> </a>
                                </div>
                                <br>
                                <div class="table">
                                    <h4 class="card-caption">Postgres SQL</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card card-profile cambiar">
                                <br>
                                <div class="card-avatar">
                                    <a> <img class="img" src="assets/img/bd/4.png"> </a>
                                </div>
                                <br>
                                <div class="table">
                                    <h4 class="card-caption">MariaDB</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
            </div>
        </div>
    </div-->
<?php //require 'partials/footer.php' 
?>