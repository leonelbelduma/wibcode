<?php require 'partials/menuprincipal.php' ?>
<div class="container">
    <br>
    <br>
    <br>
    <br>
    <br>
    <h1 align="center"><b>Contacta a WibCode</b></h1>
    <br>
    <div class="row">
        <div class="col-lg-2 col-md-2 col-sm-2"></div>
        <div class="col-lg-8 col-md-8 col-sm-8">
            <h5 align="justify">Envianos un mensaje a cualquiera de nuestras redes sociales, respondemos todas tus
                inquietudes sobre costos de proyectos y plazos de entrega.
            </h5>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-2"></div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="card card-profile">
                <br>
                <div class="table">
                    <h3>Whatsapp</h3>
                </div>
                <i class="fab fa-whatsapp red-soc fa-7x" style=" color: #299312;"> </i> <br>
                <br>
                <div class="table">
                    <a href="https://wa.me/593968768476">
                        <h4>+593 968768476</h4>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card card-profile">
                <br>
                <div class="table">
                    <h3>Gmail</h3>
                </div>
                <i class="fab fa-google red-soc fa-7x" style=" color: #FE6543;"> </i> <br>
                <br>
                <div class="table">
                    <a>
                        <h4>bwibcode@gmail.com</h4>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card card-profile">
                <br>
                <div class="table">
                    <h3>Whatsapp</h3>
                </div>
                <i class="fab fa-whatsapp red-soc fa-7x" style=" color: #299312;"> </i> <br>
                <br>
                <div class="table">
                    <a href="https://wa.me/593959999826">
                        <h4>+593 959999826</h4>
                    </a>
                </div>
            </div>
        </div>
        <br>
        <br>
        <br>
        <br>
    </div>
</div>

<?php require 'partials/footer.php' ?>