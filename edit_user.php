
 
 <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

<?php 
 require 'partials/menuprincipal.php';

if (!isset($_SESSION['user_id'])) {
    header('Location: index.php');
  }

require 'conexion.php';



if(isset($_POST['update'])){

    $pass = $_POST['password'];
    $pass1 = $_POST['password_confirmation'];
 
  if ($pass == $pass1) {
        $pass_cifrado = password_hash($pass,PASSWORD_DEFAULT);
        mysqli_query($conexion,("UPDATE  users SET names='$_POST[name]', email='$_POST[email]',
        password='$pass_cifrado'   WHERE  id ='$_POST[id]' ")); 
        $message = 'Usuario Actualizado Correctamente';
  }else {
        $message = 'Contraseñas no coinciden';
        }
    }

    
    $id_user = $_SESSION['user_id'];

       
        $consulta = "SELECT * FROM  users  WHERE  id = $id_user";
        $resultado =  mysqli_query($conexion,$consulta);
        while( $fila = mysqli_fetch_array($resultado) )  {
            $idu =  $fila['id'];
            $names = $fila['names']; 
            $email =  $fila['email']; 
           
            
        }   
    


?>


<div class="container"><br><br>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Editar Usuario</div>

                <div class="card-body">
                    <form action="#" method="POST" >
                        <input type="hidden" name="id" value="<?php if($resultado) echo $idu; ?>">
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Nombre</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="<?php if($resultado) echo $names; ?>" required autocomplete="name" autofocus>                            
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="<?php if($resultado) echo $email; ?>" required autocomplete="email">                              
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <?php if(!empty($message)): ?>                    

                            <div class="alert alert-success mt-3">
                            <p> <?= $message ?></p>


                            </div> 
                            <?php endif; ?> 

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" name="update" class="btn btn-primary">
                                    Actualizar
                                </button>
                            </div>
                        </div>

                        
                        
                    </form>
                </div>
            </div><br><br>
        </div>
    </div>
</div>

<?php require 'partials/footer.php' ?>