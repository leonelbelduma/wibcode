
 
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

<?php 
//session_start();

 require 'partials/menuprincipal.php';
 
 if (!isset($_SESSION['user_id'])) {
   // header('Location: index.php');
     echo "<script>location.href=' index.php?removido=true';</script>";
  }
 


require 'conexion.php';
if(isset($_POST['registrar'])){
    $pass = $_POST['password'];
    $pass1 = $_POST['password_confirmation'];
  if ($pass == $pass1) {

    $pass_cifrado = password_hash($pass,PASSWORD_DEFAULT);
    mysqli_query($conexion,("INSERT INTO users (names,email,password) 
    VALUES ('$_POST[name]', '$_POST[email]','$pass_cifrado')"));
    $message = 'Usuario registrado';

  }else {
    $message = 'Contraseñas no coinciden';
  }
}

if(isset($_GET['id'])){
  $id=(int) $_GET['id'];
  mysqli_query($conexion,("DELETE FROM users WHERE id=$id")); 
  $message = 'Registro Eliminado correctamente';
}


?>


<div class="container"><br><br>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Registrar Usuario</div>
                <div class="card-body">
                    <form action="#" method="POST" >
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Nombre</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="" required autocomplete="name" autofocus>                              
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="" required autocomplete="email">

                               
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required autocomplete="new-password">

                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <?php if(!empty($message)): ?>                    

                            <div class="alert alert-success mt-3">
                            <p> <?= $message ?></p>


                            </div> 
                            <?php endif; ?> 

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" name="registrar" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>

                        
                        
                    </form>
                </div>
            </div><br><br>
        </div>
    </div>
</div>

<?php require 'partials/footer.php' ?>