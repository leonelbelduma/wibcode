
 <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">


<?php 

require 'partials/menuprincipal.php' ;

require 'conexion.php';


if(isset($_POST['btn_grabar'])){     
    if (!empty($_POST['Title']) && !empty($_POST['link_descarga'])) {  
        try{
            mysqli_query($conexion,("UPDATE  proyectos SET title='$_POST[Title]', description='$_POST[descripcion]', link_video='$_POST[link_video]', 
            link_descarga='$_POST[link_descarga]',precio='$_POST[costo]'  WHERE  id ='$_POST[idp]' ")); 
            $message = 'Proyecto actualizado correctamente';
            //header('Location: contribuyentes.php');
        }catch( Exception  $ex){
          $message = 'Error durante la actualizacion';
         echo "Error al crear contribuyente";
        }
    }
}

if(isset($_GET['id'])){
    $id= (int) $_GET['id'];
    $consulta = "SELECT * FROM  proyectos  WHERE  id = $id ";
    $resultado =  mysqli_query($conexion,$consulta);
    while( $fila = mysqli_fetch_array($resultado) )  {
        $id_proy =  $fila['id'];
        $title = $fila['title']; 
        $descrip =  $fila['description']; 
        $linkvideo =  $fila['link_video']; 
        $linkdescarga = $fila['link_descarga'];
        $costo = $fila['precio'];
        
    }   
}



?>

<style>
.black{
    text-align: center;
}
.center{
    text-align: center;

}

</style>


<div class="container">
<h3 class="mb-3 pt-3 black">Editar Proyecto : <?php if($resultado) echo $title; ?></h3>

<form action="" method="POST">
   
    <div class="form-group">
        <input type="hidden" name="idp" id="idp" value="<?php if($resultado) echo $id; ?>" class="form-control">
    </div>

    <div class="form-group">
        <label for=""><b>Título</b> </label>
        <input type="text" name="Title" id="Title" value="<?php if($resultado) echo $title; ?>" class="form-control">
    </div>
    <div class="form-group">
        <label for=""><b>Descripción</b> </label>
        <input type="text" name="descripcion" id="descripcion" value="<?php if($resultado) echo $descrip; ?>" class="form-control">
    </div>
    <div class="form-group">
        <label for=""><b>Link del Video</b> </label>
        <input type="text" name="link_video" id="link_video" value="<?php if($resultado) echo $linkvideo; ?>" class="form-control">
    </div>
    <div class="form-group">
        <label for=""> <b>Link de Descarga</b> </label>
        <input type="text" name="link_descarga" id="link_descarga" value="<?php if($resultado) echo $linkdescarga; ?>" class="form-control">
    </div>   

    <div class="form-group">
        <label for=""> <b>Costo</b> </label>
        <input type="number" name="costo" id="costo" value="<?php if($resultado) echo $costo; ?>" class="form-control">
    </div>  

    <div class="center">
    
        <button type="submit" name="btn_grabar"  class="btn btn-success">Actualizar Proyecto</button>
    
    </div>
    
    
</form>
    <div class="center">
        <a href="addproyec.php"  class="btn btn-warning">  Ver Proyectos  </a>
    </div>
    


    <?php if(!empty($message)): ?>   
        <div class="alert alert-success mt-3">
        <p> <?= $message ?></p>
        </div> 
    <?php endif; ?>
</div>
<br><br><br><br><br><br>

<?php require 'partials/footer.php' ?>            
