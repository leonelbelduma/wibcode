<footer class="page-footer font-small blue-gradient pt-4">
    <div class="container-fluid text-center text-md-left">
        <div class="row">
            <div class="col-lg-4 col-md-4">
                <h5 align="center" class="text-uppercase">WibCode</h5>
                <h6 align="center">¿Qué Hacemos?<br>Asesoramiento en proyectos de programación.</h6>
            </div>
            <hr class="clearfix w-100 d-md-none pb-3">
            <div class="col-lg-4 col-md-4">
                <h5 align="center" class="text-uppercase">Contactanos</h5>
                <ul class="list-unstyled">
                    <li>
                        <p><a href="https://wa.me/593968768476" target="_blank"><h6 align="center"><i class="fab fa-whatsapp"></i> +593 968768476</h6></a></p>
                    </li>
                    <li>
                        <p><a href="https://wa.me/593959999826" target="_blank"><h6 align="center"><i class="fab fa-whatsapp"></i> +593 959999826</h6></a></p>
                    </li>
                    <li>
                        <h6 align="center"><i class="fab fa-google list"></i> bwibcode@gmail.com</h6>
                    </li>
                    <li>
                </ul>
            </div>
            <div class="col-lg-4 col-md-4">
                <h5 align="center" class="text-uppercase"><b>Redes Sociales</b></h5>
                <ul class="list-unstyled">
                    <li>
                        <p><a href="https://www.facebook.com/wibcode" target="_blank"><h6 align="center"><i class="fab fa-facebook"></i> Facebook</h6></a></p>
                    </li>
                    <li>
                        <p><a href="https://www.youtube.com/channel/UC09NrKiC90RnVicikBfJDMA" target="_blank"><h6 align="center"><i class="fab fa-youtube"></i> Youtube</h6></a></p>
                    </li>
                    <li>
                        <p><a href="https://www.instagram.com/wibcode" target="_blank"><h6 align="center"><i class="fab fa-instagram"></i> Instagram</h6></a></p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer-copyright text-center py-3">© 2020 Copyright:
        <a href="www.wibcode.com"> WibCode</a>
    </div>
</footer>