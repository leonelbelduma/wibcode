<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">

<?php

require 'partials/menuprincipal.php';
require 'conexion.php';
$consulta = "SELECT * FROM  proyectos ORDER BY id ASC";
$resultado =  mysqli_query($conexion, $consulta);
?>

<div class="container">
    <br>
    <br>
    <br>
    <br>
    <br>
    <div class="row">
        <?php while ($fila = mysqli_fetch_array($resultado)) {  ?>
            <div class="col-lg-6">
                <div class="card">
                    <form action="" method="post">
                        <div class="card-header">
                            <h4 class="justify"><?php echo $fila['id']; ?>.- <?php echo $fila['title'];  ?> </h4>
                        </div>
                        <div class="card-body">
                            <p class="justify"><?php echo $fila['description']; ?></p><br>
                            <?php $precio = $fila['precio'];

                            $estado = 'none';
                            $estadogratis = '';

                            ?>
                            <div class="text-center">
                                <iframe width="380" height="235" src="<?php echo $fila['link_video']; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                <?php if ($precio > 0) {
                                    $estado = '';
                                    $estadogratis = 'none';
                                }   ?>

                                <br><br> 
                                <a href="<?php echo $fila['link_descarga']; ?>" target="_blank" class="btn btn-naranja" style="display: <?php echo $estadogratis ?>"><i class="fas fa-cloud-download-alt"></i> <b>DESCARGAR</b></a>
                                <a href="vntaproyec.php?id=<?php echo $fila['id']; ?>" target="_blank" class="btn btn-comprar" style="display: <?php echo $estado ?>"><i class="fas fa-shopping-cart"></i> <b>COMPRAR $<?php echo $precio   ?></b></a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        <?php } ?>
    </div>
</div>
<?php require 'partials/footer.php' ?>