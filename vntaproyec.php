<?php
require 'conexion.php';
if (isset($_GET['id'])) {
    $id = (int) $_GET['id'];
    $consulta = "SELECT * FROM  proyectos  WHERE  id = $id ";
    $resultado =  mysqli_query($conexion, $consulta);
    while ($fila = mysqli_fetch_array($resultado)) {
        $id_proy =  $fila['id'];
        $title = $fila['title'];
        $descrip =  $fila['description'];
        $linkvideo =  $fila['link_video'];
        $linkdescarga = $fila['link_descarga'];
        $costo = $fila['precio'];
    }
}
?>

<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">

<?php
require 'partials/menuprincipal.php';
?>

<div class="container">
    <br>
    <br>
    <br>
    <br>
    <h5 align="center">
        <b>Usted eligio comprar: </b><?php echo $title ?>
    </h5>
    <hr>
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    <h4 align="center"><b> Paypal </b></h4>
                </div>
                <div class="card-body">
                    <a href="<?php echo $linkdescarga ?> ">
                        <h3 align="center"><i class="fab fa-cc-paypal fa-6x"></i></h3>
                    </a>
                    <br>
                    <p align="justify">
                        Para realizar el pago da clic en el boton.</a>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    <h4 align="center"><b> Wester Union </b></h4>
                </div>
                <div class="card-body pago">
                    <img src="assets/img/western-union.png">
                    <br>
                    <p align="justify">
                        Para realizar el pago por Wester Union, envia un mensaje al siguiente correo <b>bwibcode@gmail.com</b> o escribenos a <a target="_blank" href="https://wa.me/593968768476">+593 968768476</a>.
                    </p>
                </div>
            </div>
        </div>
        <div class="col-md-2"></div>
    </div>
</div>